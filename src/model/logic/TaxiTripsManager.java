package model.logic;

import java.io.*; 
import java.util.Iterator;

import com.google.gson.*;

import api.ITaxiTripsManager;
import model.vo.Taxi;
import model.vo.VOTaxiTrip;
import model.data_structures.DoublyLinkedList;
import model.data_structures.LinkedList;
import model.data_structures.Node;
import model.vo.Service;

public class TaxiTripsManager implements ITaxiTripsManager {

	// TODO
	// Definition of data model 
	DoublyLinkedList<VOTaxiTrip> listaServicios;
	DoublyLinkedList<Taxi> taxis;

	public void loadServices (String serviceFile) {
		// TODO Auto-generated method stub
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(serviceFile));
			Gson gson = new GsonBuilder().create();
			VOTaxiTrip[] servicios = gson.fromJson(reader, VOTaxiTrip[].class);
			listaServicios= new DoublyLinkedList<>();
			taxis= new DoublyLinkedList<>();
			for(VOTaxiTrip actual: servicios)
			{
				actual.quitarEspacios();
				listaServicios.add(actual);

			}
			for(VOTaxiTrip actual: servicios)
			{

				if(!isTaxi(actual.getTaxi_id(), taxis))
				{
					Taxi agregar= new Taxi(actual.getTaxi_id(), actual.getCompany());
					taxis.add(agregar);
				}

			}
		} catch (FileNotFoundException ex) {
			ex.getStackTrace();
		}
		System.out.println("Inside loadServices with " + serviceFile);

	}
	public boolean isTaxi(String id, DoublyLinkedList<Taxi> lista)
	{
		boolean ans=false;
		for(Taxi actual: lista)
		{
			if(actual!=null)
			{
				if(actual.getTaxiId().equals(id))
				{
					ans=true;
				}
			}
		}
		return ans;
	}
	@Override
	public LinkedList<Taxi> getTaxisOfCompany(String company) {
		// TODO Auto-generated method stub
		System.out.println("Inside getTaxisOfCompany with " + company);
		Iterator iterador= taxis.iterator();
		LinkedList<Taxi> ans= new LinkedList<>();
		while(iterador.hasNext())
		{
			Taxi actual= (Taxi)iterador.next();
			
				if(company.equals(actual.getCompany()))
				{
					ans.insert(actual);
					System.out.println(actual.getTaxiId());
					System.out.println();
				}
			
		}
		return ans;

	}

	@Override
	public LinkedList<VOTaxiTrip> getTaxiServicesToCommunityArea(int communityArea) {
		// TODO Auto-generated method stub
		System.out.println("Inside getTaxiServicesToCommunityArea with " + communityArea);
		Iterator iterador= listaServicios.iterator();
		LinkedList<VOTaxiTrip> ans= new LinkedList<VOTaxiTrip>();
		while(iterador.hasNext())
		{
			VOTaxiTrip actual= (VOTaxiTrip)iterador.next();
			if(actual.getDropoff_community_area()==communityArea)
			{
				ans.insert(actual);
				
			}
		}
	
		return ans;

	}


}
