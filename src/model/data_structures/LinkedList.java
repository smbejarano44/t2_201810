package model.data_structures;

import java.util.ListIterator; 
import java.util.NoSuchElementException;




public class LinkedList<T>  implements Iterable<T> {

	private Node first  ;
	private int size ;

	/**
	 * Insert at the front of the list
	 * @param node
	 */
	public void insert(T thing) {
		Node node= new Node<T>(thing);
		node.setSiguiente(first);
		first = node;
		size++;
	}
	
	public boolean has(T thing)
	{
		boolean ans=false;
		ListIterator iterador= iterator();
		while(iterador.hasNext())
		{
			T actual= (T)iterador.next();
			if(actual.equals(thing))
			{
				ans=true;
			}
		}
		return ans;
	}

	/**
	 * Remove from the front of the list
	 * @param node
	 */
	public void remove(int i){
		Node borrar= get(i);
		Node anterior= get(i-1);
		if(i==0&&first.darSiguiente()!=null)
			first = first.darSiguiente();
		else if(borrar!=null&&borrar.darSiguiente()!=null)
			anterior.setSiguiente(borrar.darSiguiente());
		else first = null;
	}
	public Node<T> get(int i)
	{
		if(i<0)
		{
			i= i*-1;
		}
		if(i>size)
		{
			return null;
		}
		int contador=0;
		Node actual=first;
		while(first.darSiguiente()!=null&&contador!=i)
		{
			actual=first.darSiguiente();
			contador++;
		}
		return actual;
	}



	   public ListIterator<T> iterator()  { return new LinkedListIterator(); }

	    // assumes no calls to DoublyLinkedList.add() during iteration
	    private class LinkedListIterator implements ListIterator<T> {
	        private Node current      = first;  // the node that is returned by next()
	        private Node lastAccessed = null;      // the last node to be returned by prev() or next()
	                                               // reset to null upon intervening remove() or add()
	        private int index = 0;

	        public boolean hasNext()      { return index < size; }
	        public int nextIndex()        { return index;     }

	        public T next() {
	            if (!hasNext()) throw new NoSuchElementException();
	            lastAccessed = current;
	            T item = (T)current.darActual();
	            current = current.darSiguiente(); 
	            index++;
	            return item;
	        }

	        
  
	        // remove the element that was last accessed by next() or previous()
	        // condition: no calls to remove() or add() after last call to next() or previous()
	        public void remove() { 
	            if (lastAccessed == null) throw new IllegalStateException();
	            Node x = lastAccessed.darAnterior();
	            Node y = lastAccessed.darSiguiente();
	            x.setSiguiente(y);;
	            
	            size--;
	            if (current == lastAccessed)
	                current = y;
	            else
	                index--;
	            lastAccessed = null;
	        }

	        // add element to list 
	        public void add(T item) {
	            Node x = current.darAnterior();
	            Node y = new Node(item);
	            Node z = current;
	            y.setObjeto(item);
	            x.setSiguiente(y);
	            y.setSiguiente(z);
	            size++;
	            index++;
	            lastAccessed = null;
	        }
			@Override
			public boolean hasPrevious() {
				// TODO Auto-generated method stub
				return false;
			}
			@Override
			public T previous() {
				// TODO Auto-generated method stub
				return null;
			}
			@Override
			public int previousIndex() {
				// TODO Auto-generated method stub
				return 0;
			}
			@Override
			public void set(T arg0) {
				// TODO Auto-generated method stub
				
			}

	    }



}
