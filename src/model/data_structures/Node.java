package model.data_structures;

public class Node<T> {
	private T objeto;
	private Node<T> siguiente;
	private Node<T> anterior;
	public Node(T ob)
	{
		objeto=ob;
	}
	public T darActual()
	{
		return objeto;
	}
	public Node<T> darSiguiente()
	{
		return siguiente;
	}
	public Node<T> darAnterior()
	{
		return anterior;
	}
	public void setObjeto(T objeto) {
		this.objeto = objeto;
	}
	public void setSiguiente(Node<T> siguiente) {
		this.siguiente = siguiente;
	}
	public void setAnterior(Node<T> anterior) {
		this.anterior = anterior;
	}


}
