package esqueleto_t2_201720;

import static org.junit.Assert.*;

import org.junit.Test;
import model.data_structures. Node;
 
 
public class NodeTest {
	private Node nodo;
	public void setupEscenario1( )
	{
		String prueba="pruebaaa";
		nodo = new Node(prueba);
		
	}
	@Test
	public void testsetSiguiente()
	{
		setupEscenario1();
		assertNull(nodo.darSiguiente());
		Node siguiente= new Node("Siguiente");
		nodo.setSiguiente(siguiente);
		assertEquals(siguiente, nodo.darSiguiente());
	}
	@Test
	public void testsetAnterior()
	{setupEscenario1();
		assertNull(nodo.darSiguiente());
		Node anterior= new Node("Anterior");
		nodo.setAnterior(anterior);
		assertEquals(anterior, nodo.darAnterior());
	}
	@Test
	public void testdarObjeto()
	{
		setupEscenario1();
		assertEquals("pruebaaa", nodo.darActual());
	}
}
